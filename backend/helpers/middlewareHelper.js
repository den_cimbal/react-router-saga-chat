const { BadRequest } = require('./customErrors');

exports.checkRequiredFields = (fields, model, errors) => {
  for (const field in model) {
    if (!fields.hasOwnProperty(field)) {
      errors.push(`The field '${field}' is required.`);
    }
  }
};

exports.validateFields = (fields, errors) => {
  for (const field in fields) {
    if (field === 'avatar' || field === 'likes') {
      return;
    }
    const fieldValue = fields[field];
    if (!fieldValue || fieldValue.trim().length === 0) errors.push(`The value of field '${field}' is invalid.`);
  }
};

exports.checkErrors = errors => {
  if (errors.length) {
    throw new BadRequest(errors.join(' '));
  }
};
