const { Router } = require('express');
const MessageService = require('../services/messageService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createMessageValid, updateMessageValid } = require('../middlewares/message.validation.middleware');

const router = Router();

router.get('/', getMessages, responseMiddleware);

router.get('/:id', getMessage, responseMiddleware);

router.post('/', createMessageValid, createMessage, responseMiddleware);

router.put('/:id', updateMessageValid, updateMessage, responseMiddleware);

router.delete('/:id', deleteMessage, responseMiddleware);

function getMessages(req, res, next) {
  try {
    const messages = MessageService.getMessages();
    res.data = messages;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}

function getMessage(req, res, next) {
  try {
    const id = req.params.id;
    const message = MessageService.getMessage(id);
    res.data = message;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}

function createMessage(req, res, next) {
  if (res.err) {
    return next();
  }
  try {
    const data = req.body;
    const createdMessage = MessageService.createMessage(data);
    res.data = createdMessage;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}

function updateMessage(req, res, next) {
  if (res.err) {
    return next();
  }
  try {
    const id = req.params.id;
    const data = req.body;
    const updatedMessage = MessageService.updateMessage(id, data);
    res.data = updatedMessage;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}

function deleteMessage(req, res, next) {
  try {
    const id = req.params.id;
    const deletedMessage = MessageService.deleteMessage(id);
    res.data = deletedMessage;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}

module.exports = router;
