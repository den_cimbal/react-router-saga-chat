const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', getUsers, responseMiddleware);

router.get('/:id', getUser, responseMiddleware);

router.post('/', createUserValid, createUser, responseMiddleware);

router.put('/:id', updateUserValid, updateUser, responseMiddleware);

router.delete('/:id', deleteUser, responseMiddleware);

function getUsers(req, res, next) {
  try {
    const users = UserService.getUsers();
    res.data = users;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}

function getUser(req, res, next) {
  try {
    const id = req.params.id;
    const user = UserService.getUser(id);
    res.data = user;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}

function createUser(req, res, next) {
  if (res.err) {
    return next();
  }
  try {
    const data = req.body;
    const createdUser = UserService.createUser(data);
    res.data = createdUser;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}

function updateUser(req, res, next) {
  if (res.err) {
    return next();
  }
  try {
    const id = req.params.id;
    const data = req.body;
    const updatedUser = UserService.updateUser(id, data);
    res.data = updatedUser;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}

function deleteUser(req, res, next) {
  try {
    const id = req.params.id;
    const deletedUser = UserService.deleteUser(id);
    res.data = deletedUser;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}

module.exports = router;
