const { MessageRepository } = require('../repositories/messageRepository');
const { NotFound } = require('../helpers/customErrors');

class MessageService {
  getMessages() {
    const messages = MessageRepository.getAll();
    if (!messages) {
      throw new NotFound('Messages not found');
    }
    return messages;
  }

  getMessage(id) {
    const message = this.search({ id });
    if (!message) {
      throw new NotFound('Message not found');
    }
    return message;
  }

  createMessage(data) {
    const message = { ...data, likes: [] };
    const createdMessage = MessageRepository.create(message);
    return createdMessage;
  }

  updateMessage(id, data) {
    const messageToUpdate = this.search({ id });
    if (!messageToUpdate) {
      throw new NotFound('Message cannot be updated, because message not found');
    }
    const updatedMessage = MessageRepository.update(id, data);
    return updatedMessage;
  }

  deleteMessage(id) {
    const messageToDelete = this.search({ id });
    if (!messageToDelete) {
      throw new NotFound('Message cannot be deleted, because message not found');
    }
    const deletedMessage = MessageRepository.delete(id);
    return deletedMessage;
  }

  search(search) {
    const message = MessageRepository.getOne(search);
    if (!message) {
      return null;
    }
    return message;
  }
}

module.exports = new MessageService();
