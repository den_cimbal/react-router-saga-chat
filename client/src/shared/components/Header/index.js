import React from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faComments } from "@fortawesome/free-solid-svg-icons";

import './header.css';

const Header = (props) => {
  const {
    logOut,
    profile: { login, avatar }
  } = props;
  return (
    <header className="header">
      <div className="header__container">
        <div className="header__left">
          <FontAwesomeIcon className="header__logo" icon={ faComments } />
        </div>
        <div className="header__links">
          {login === 'admin' && (
            <>
              <Link to="/chat">Chat</Link>
              <Link to="/users">Users</Link>
            </>
          )}
        </div>
        <div className="header__right">
          <button type="button" onClick={logOut} className="header__logout">
            Logout
          </button>
          <img className="header__avatar" src={avatar} alt="avatar" />
        </div>
      </div>
    </header>
  );
};

export default Header;
