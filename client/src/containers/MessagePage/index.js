import React, { useState, useEffect } from 'react';

import { connect } from 'react-redux';
import { fetchMessage } from './actions';
import { updateMessage } from '../ChatPage/actions';

import Loader from '../../shared/components/Loader';
import Error from '../../shared/components/Error';

import './index.css';

const MessagePage = (props) => {
  let [value, setValue] = useState('');
  const { id } = props.match.params;
  const { fetchMessage, updateMessage, message } = props;

  useEffect(() => {
    if (id) {
      fetchMessage(id);
    }
  }, [id, fetchMessage]);

  useEffect(() => {
    if (props.message.data.text) {
      setValue(props.message.data.text);
    }
  }, [props.message.data]);

  const onCancel = () => {
    props.history.push('/chat');
  };

  const onChange = ({ target }) => {
    const value = target.value;
    setValue(value);
  };

  const onSubmit = (event) => {
    event.preventDefault();
    const formatedValue = value.trim();
    if (!formatedValue) {
      return;
    } else {
      updateMessage(message.data.id, { text: formatedValue });
      props.history.push('/chat');
    }
  };

  return (
    <div className="modal-wrapper">
      {message.error && <Error message={message.error} onModalClose={onCancel} />}
      <div className="modal">
        {message.isLoading ? (
          <Loader />
        ) : (
          <>
            <h3 className="modal__header">Edit message</h3>
            <form onSubmit={onSubmit}>
              <div className="modal__content" value={value}>
                <textarea placeholder="Type your edited message here..." value={value} onChange={onChange}></textarea>
              </div>
              <div className="modal__buttons">
                <button className="modal__button modal__button--cancel" onClick={onCancel}>
                  Cancel
                </button>
                <button className="modal__button" type="submit">
                  Update
                </button>
              </div>
            </form>
          </>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    message: state.message
  };
};

const mapDispatchToProps = {
  fetchMessage,
  updateMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(MessagePage);
