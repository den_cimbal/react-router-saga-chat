import { getReq } from '../../shared/helpers/requestHelper';

export const getMessage = async (id) => {
  return await getReq('messages', id);
};
