import { getReq, postReq, putReq } from '../../shared/helpers/requestHelper';

export const getUser = async (id) => {
  return await getReq('users', id);
};

export const createUser = async (user) => {
  return await postReq('users', user);
};

export const changeUser = async (id, updatedUser) => {
  return await putReq('users', id, updatedUser);
};
