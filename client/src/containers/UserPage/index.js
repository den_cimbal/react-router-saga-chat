import React, { useState, useEffect } from 'react';

import TextInput from '../../shared/components/TextInput';

import { connect } from 'react-redux';
import { fetchUser, updateUser, addUser, leaveUserPage } from './actions';

import Error from '../../shared/components/Error';
import Loader from '../../shared/components/Loader';
import Success from '../../shared/components/Success';

import './index.css';

const UserPage = (props) => {
  const { fetchUser, updateUser, addUser, leaveUserPage, user } = props;

  const [isEdited, setIsEdited] = useState(false);
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');
  const [name, setName] = useState('');
  const [avatar, setAvatar] = useState('');

  useEffect(() => {
    if (props.match.params.id) {
      fetchUser(props.match.params.id);
      setIsEdited(true);
    }
  }, [props.match.params.id, fetchUser]);

  useEffect(() => {
    if (props.user.data && props.match.params.id) {
      const { login, password, avatar, name } = props.user.data;
      setLogin(login);
      setPassword(password);
      setName(name);
      setAvatar(avatar);
    }
  }, [props.user.data, props.match.params.id]);

  const onSubmit = (event) => {
    event.preventDefault();
    if (isEdited) {
      updateUser(user.data.id, { login, password, name, avatar });
    } else {
      addUser({ login, password, name, avatar });
    }
  };

  const onLeaveUserPage = () => {
    setLogin('');
    setPassword('');
    setName('');
    setAvatar('');
    leaveUserPage();
    props.history.push('/users');
  };

  const onLoginChange = ({ target }) => {
    const value = target.value;
    setLogin(value);
  };

  const onPasswordChange = ({ target }) => {
    const value = target.value;
    setPassword(value);
  };

  const onAvatarChange = ({ target }) => {
    const value = target.value;
    setAvatar(value);
  };

  const onNameChange = ({ target }) => {
    const value = target.value;
    setName(value);
  };

  const isDisabled = !login.trim().length || !password.trim().length || !avatar.trim().length || !name.trim().length;

  return (
    <div className="user-page">
      {user.notFound && <Error message={user.notFound} onModalClose={onLeaveUserPage} />}
      {user.error && <Error message={user.error} />}
      {user.success && <Success message={user.success} onModalClose={onLeaveUserPage} />}
      {isEdited && user.isLoading ? (
        <Loader />
      ) : (
        <>
          <h3 className="user-page__header">{isEdited ? 'Update user' : 'Create user'}</h3>
          <form onSubmit={onSubmit}>
            <TextInput type={'text'} onChange={onLoginChange} text={login} label={'Login'} />
            <TextInput type={'password'} onChange={onPasswordChange} text={password} label={'Password'} />
            <TextInput type={'text'} onChange={onAvatarChange} text={avatar} label={'Avatar'} />
            <TextInput type={'text'} onChange={onNameChange} text={name} label={'Name'} />
            <div className="user-page__buttons">
              <button className="user-page__button user-page__button--cancel" onClick={onLeaveUserPage}>
                Cancel
              </button>
              <button className="user-page__button" type="submit" disabled={isDisabled}>
                {isEdited ? 'Update' : 'Create'}
              </button>
            </div>
          </form>
        </>
      )}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.user
  };
};

const mapDispatchToProps = {
  fetchUser,
  updateUser,
  addUser,
  leaveUserPage
};

export default connect(mapStateToProps, mapDispatchToProps)(UserPage);
