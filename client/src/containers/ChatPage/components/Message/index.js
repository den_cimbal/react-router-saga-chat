import React from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart, faEdit, faTrashAlt } from "@fortawesome/free-solid-svg-icons";

import { checkIsLiked } from '../../helper';

import './message.css';

export const Message = (props) => {
  const { currentUserId, text, userId, user, avatar, time, likes, onDelete, onEdit, onLike } = props;
  const isOwnMessage = currentUserId === userId;
  const messageClassNames = isOwnMessage ? 'message message--own' : 'message message--users';
  const likeClassNames = checkIsLiked(likes, currentUserId) ? 'message__like message__like--active' : 'message__like';

  return (
    <div className={messageClassNames}>
      <div className="message__body">
        {!isOwnMessage && <img className="message__avatar" src={avatar} alt={user} />}
        <div className="message__content">
          <p className="message__text">{text}</p>
          <p className="message__time">{time}</p>
        </div>
      </div>
      <div className="message__controls">
        <span className={likeClassNames}>
          <span onClick={!isOwnMessage ? onLike : undefined}><FontAwesomeIcon icon={ faHeart } /></span>
          {likes.length}
        </span>
        {isOwnMessage && (
          <>
            <span className="message__delete" onClick={onEdit}>
              <FontAwesomeIcon icon={ faEdit } />
            </span>
            <span className="message__delete" onClick={onDelete}>
              <FontAwesomeIcon icon={ faTrashAlt } />
            </span>
          </>
        )}
      </div>
    </div>
  );
};

export default Message;
