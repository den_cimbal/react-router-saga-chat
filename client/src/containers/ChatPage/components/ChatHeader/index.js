import React from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUsers, faComments, faHourglass } from "@fortawesome/free-solid-svg-icons";

import './chatHeader.css';

export const ChatHeader = ({ chatName, usersCount, messagesCount, lastMessageTime }) => {
  return (
    <header className="chat-header">
      <div className="chat-header__left">
        <h3 className="chat-header__name">{chatName}</h3>
        <div className="chat-header__users">
          <p>
            <FontAwesomeIcon icon={ faUsers } /> Users
          </p>
          <p>{usersCount}</p>
        </div>
        <div className="chat-header__messages">
          <p>
            <FontAwesomeIcon icon={ faComments } /> Messages
          </p>
          <p>{messagesCount}</p>
        </div>
      </div>
      <div className="chat-header__time">
        <p>
          <FontAwesomeIcon icon={ faHourglass } /> Last message
        </p>
        <p>{lastMessageTime}</p>
      </div>
    </header>
  );
};

export default ChatHeader;
