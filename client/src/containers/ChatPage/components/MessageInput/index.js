import React, { useState } from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHandPointRight } from "@fortawesome/free-solid-svg-icons";

import './messageInput.css';

export const MessageInput = (props) => {
  let [value, setValue] = useState('');

  const handleChange = ({ target }) => {
    const value = target.value;
    setValue(value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (!value) {
      return;
    }
    const { onMessageAdd, user, avatar, userId } = props;
    const newMessage = { text: value, user, avatar, userId };
    onMessageAdd(newMessage);
    setValue('');
  };

  return (
    <div className="message-input">
      <form onSubmit={handleSubmit}>
        <textarea placeholder="Type your message here..." onChange={handleChange} value={value}></textarea>
        <div className="message-input__buttons">
          <button type="submit"><FontAwesomeIcon icon={ faHandPointRight } /></button>
        </div>
      </form>
    </div>
  );
};

export default MessageInput;
