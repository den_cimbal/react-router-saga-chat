import { FETCH_USERS, FETCH_USERS_ERROR, FETCH_USERS_SUCCESS } from './actionTypes';

const initialState = {
  isLoading: true,
  error: false,
  data: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_USERS: {
      return { ...state, error: false, isLoading: true };
    }
    case FETCH_USERS_ERROR: {
      const { error } = action.payload;
      return {
        ...state,
        isLoading: false,
        error
      };
    }
    case FETCH_USERS_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        isLoading: false,
        error: false,
        data
      };
    }

    default:
      return state;
  }
}
