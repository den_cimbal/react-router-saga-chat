import { getReq, deleteReq } from '../../shared/helpers/requestHelper';

export const getUsers = async () => {
  return await getReq('users');
};

export const removeUser = async (id) => {
  return await deleteReq('users/', id);
};
