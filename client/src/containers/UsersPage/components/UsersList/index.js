import React from 'react';
import UserItem from '../UserItem';

import './index.css';

export const UsersList = (props) => {
  const { users, onDeleteUser, onUserUpdate } = props;

  const userItems = users.map((user) => {
    const { id, ...userProps } = user;
    return <UserItem key={id} {...userProps} onDelete={() => onDeleteUser(id)} onUpdate={() => onUserUpdate(id)} />;
  });
  return <div className="users-container">{userItems}</div>;
};

export default UsersList;
