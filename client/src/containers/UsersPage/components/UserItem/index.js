import React from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faTrashAlt } from "@fortawesome/free-solid-svg-icons";

import './userItem.css';

const UserItem = (props) => {
  const { avatar, name, onDelete, onUpdate } = props;
  return (
    <div className="user-item">
      <div className="user-item__item">
        <img src={avatar} alt={name} />
      </div>
      <div className="user-item__item">
        <p>{name}</p>
      </div>
      <div className="user-item__item">
        <span onClick={onUpdate} className="user-item__icon"><FontAwesomeIcon icon={ faEdit } /></span>
      </div>
      <div className="user-item__item">
        <span onClick={onDelete} className="user-item__icon"><FontAwesomeIcon icon={ faTrashAlt } /></span>
      </div>
    </div>
  );
};

export default UserItem;
