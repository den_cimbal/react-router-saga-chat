import { call, put, takeEvery, all } from 'redux-saga/effects';
import { LOGIN_USER, LOGIN_USER_SUCCESS, LOGIN_USER_ERROR } from './actionTypes';
import { login } from './service';

export function* loginUser(action) {
  try {
    const userData = yield call(login, action.payload.userData);
    yield put({ type: LOGIN_USER_SUCCESS, payload: { data: userData } });
  } catch (error) {
    yield put({ type: LOGIN_USER_ERROR, payload: { error: error.message } });
  }
}

function* watchLoginUser() {
  yield takeEvery(LOGIN_USER, loginUser);
}

export default function* loginSagas() {
  yield all([watchLoginUser()]);
}
